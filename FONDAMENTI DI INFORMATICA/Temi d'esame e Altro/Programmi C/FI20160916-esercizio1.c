/*
Il checksum, è un semplice meccanismo per controllare la correttezza di un messaggio inviato attraverso una rete informatica che consiste nel calcolare la somma
dei bit che lo compongono (che corrisponde al numero di 1 contenuti nella codifica) ed inviarla insieme al messaggio.

A.	Si definisca un tipo di dato, messaggio, in grado di contenere una stringa ed il suo valore di checksum.
B.	Si implementi la funzione, verifica, che riceve in ingresso una variabile di tipo messaggio e restituisce 1 se il checksum è corretto e 0 viceversa.

Nota. Il checksum di una stringa deve essere calcolato sommando i bit della codifica binaria di ciascun carattere che la compongono. Si suggerisce di implementare
una funzione di supporto, chksum, che riceve in ingresso un char e restituisce il checksum della sua codifica binaria.

Esempio. La stringa “hi” è composta dal carattere ‘h’ e il carattere ‘i’. La codifica ASCII di ‘h’ è 104 (1101000 in binario), mentre quella di ‘i’ è 105
(1101001 in binario). Il checksum di ‘h’ è quindi 3, mentre quello di 104 è 4. Il checksum del messaggio è quindi 3+4=7.

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char stringa[1000];
    long int checksum;
} messaggio;

int verifica(messaggio msg);
int chksum(char c);

int verifica(messaggio msg) {
    int l = strlen(msg.stringa), i;
    long int checksum = 0;

    for(i=0; i<l; i++) {
        checksum = checksum + chksum(msg.stringa[i]);
    }

    if(checksum == msg.checksum)
        return 1;
    else
        return 0;
}

int chksum(char c) {
    int sum = 0;
    while (c>0) {
        sum = sum + c%2;
        c = c / 2;
    }

    return sum;
}
