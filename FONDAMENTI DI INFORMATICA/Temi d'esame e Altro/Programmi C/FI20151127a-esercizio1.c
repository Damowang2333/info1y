/*
Si implementi una funzione conta che:
•	riceve in ingresso due stringhe, str1 e str2
•	restituisce il numero di volte che la stringa str2 compare nella stringa str1

Esempio: La chiamata conta(“treninonino”,“ni”) restituisce 2 perché la sottostringa “ni” comprare due volte in  “treninonino”

Note: Si implementi la funzione conta, ipotizzando che le stringhe str1 ed str2 contengano il carattere terminatore.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int conta(char a[], char b[]) {
    int count = 0;
    const char *tmp = a;
    while(tmp = strstr(tmp, b))
    {
       count++;
       tmp += strlen(b);
    }

    return count;
}

int main() {
    char a[] = "treniiiinono";
    char b[] = "ii";

    printf("%d\n", conta(a, b));

    printf("%f", (float)5/2);
    return 0;
}
