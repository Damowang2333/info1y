//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

int mcd_euc (int num_a, int num_b){
    
    if(num_a==num_b)
        return num_b;
    
    else if(num_a>num_b)
        return mcd_euc(num_b, num_a-num_b);
        
    else
        return mcd_euc(num_a, num_b-num_a);
    
}

int main() {
    
    int num_1,num_2;

    printf("Insert Values: ");
    
    scanf("%d%d", &num_1,&num_2);
    
    printf("L' M.C.D. tra %d e %d e': %d\n", num_1,num_2,mcd_euc(num_1, num_2));

    return 0;

}
